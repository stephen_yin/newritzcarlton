
//预置获取全站图片接口(此接口必须预先调用，接下来其它接口返回的数据中如果需要图片，需要从此接口的结果中获取。)

(function () {
	// $(".content .contentText").html(navigator.appVersion);
	var requestUrl = localStorage.apiServer + "/image/?" + $.param(BASEDATA);
	$.ajax(requestUrl).then(
		function (json) {
			
			console.log("good_img:", json);
			localStorage.imgList = JSON.stringify(json);
			getRoom();
			getHotel();
		},

		function (err) {
			console.error("Error_img: " + err.responseText);
		}
	);
})();

/**
 * 新junction接口对接
 */





//1.设备查询接口,获取房间号
function getRoom(){
	var requestUrl = localStorage.apiServer + "/device/get/?" + $.param(BASEDATA);
	$.ajax(requestUrl).then(
	    function (json) {
	        console.log("getRoom:", json);
			$(".roomnumber").html("Room:" + json.data.device.room);
			localStorage.roomNum = json.data.device.room;
			
	    },
	
	    function (err) {
	        console.error("getRoom: " + err.responseText);
	    }
	);
}

//2.获取酒店根信息(保存多语言信息，默认语言参数)
function getHotel(){
	var requestUrl = localStorage.apiServer + "/page/hotel/?" + $.param(BASEDATA);
	$.ajax(requestUrl).then(
	    function (json) {
			console.log("getHotel:", json);
			
			localStorage.defaultLanguage = json.data.defaultLanguage;
		
	        if (localStorage.defaultLanguage == "zh-cn") {
	            $("#cn").focus();
	            setCookie('focusId', i, 1);
	            $("#h31").html("尊敬的贵宾");
	            $("#h32").html("");
	            $(".sele").html('选择');
	            $(".confirm").html("确认");
	        } else {
	            $("#en").focus();
	            setCookie('focusId', i, 1);
	            $("#h31").html("Dear Valued Guest");
	            $("#h32").html(",");
	            $(".sele").html('Select');
	            $(".confirm").html("Confirm");
			}

			getUserInfo();
			getWelcomePage(json.data.defaultLanguage);
			
			
	    },
	
	    function (err) {
	        console.error("getHotel: " + err.responseText);
	    }
	);
}

//3.如果对接PMS获取设备所在房间顾客信息
function getUserInfo(){
	var requestUrl = localStorage.apiServer + "/hotel/guest_profile/?" + $.param(BASEDATA);
	$.ajax(requestUrl).then(
	    function (json) {
	        console.log("getUserInfo:", json);
	        if (json.ret === 200) {
	            if (json.data.length === 0) {
					var dear = localStorage.defaultLanguage == "zh-cn" ? "尊敬的" : "Dear ";
	                var userName = localStorage.defaultLanguage == "zh-cn" ? "贵宾 " : "Valued Guest";      
	                var h32 = localStorage.defaultLanguage == "zh-cn" ? "" : ","; 	
	                $("#h31").html(dear + userName);
	                $("#h32").html(h32);
	
	            }
	        }
	    },
	
	    function (err) {
	        console.error("getUserInfo: " + err.responseText);
	    }
	);

}

//4:对接餐厅接口
function getRestaurantInfo(){	

	BASEDATA.page_type = 'restaurant';
	BASEDATA.lang_agnostic_id = 'r001'
	var requestUrl = localStorage.apiServer + "/page/search/?" + $.param(BASEDATA);
	console.log('get_restaurant:', requestUrl);
	$.ajax(requestUrl).then(
	    function (json) {
			console.log("getRestaurantInfo:", json);
			
			delete BASEDATA.page_type;
			delete BASEDATA.lang_agnostic_id;
	
	    },
	
	    function (err) {
	        console.error("getRestaurantInfo: " + err.responseText);
	    }
	);

}


//5:对接欢迎页接口
function getWelcomePage(defaultLanguage){
	
	delete BASEDATA.page_type;
	delete BASEDATA.lang_agnostic_id
	var requestUrl = localStorage.apiServer + "/page/butler_welcomepage/?" + $.param(BASEDATA);
	console.log('butler_welcomepage:', requestUrl);
	$.ajax(requestUrl).then(
	    function (json) {
			console.log("getWelcomePage:", json);
			console.log(json.data.welcome.backgroundImage);
			console.log(json.data.welcome.backgroundVideoUrl);
			var IMGLIST = localStorage.imgList ?  JSON.parse(localStorage.imgList) : {};
	        var logo = json.data.welcome.logo != undefined ? IMGLIST.data[json.data.welcome.logo.internalImageId].internal_image_url : "";
			var backgroudimg = json.data.welcome.backgroundImage != undefined ? IMGLIST.data[json.data.welcome.backgroundImage.internalImageId].internal_image_url : "";
			var backgroundVideoUrl = json.data.welcome.backgroundVideoUrl != undefined ? json.data.welcome.backgroundVideoUrl : "";
			//判断:如果欢迎页存在视频URL时,尝试调用安卓的VOD播放接口播放视频. 如果存在视频,则优先使用视频,
			if(backgroundVideoUrl != ""){
				try {
					$("body").css("background","transparent");
					window.JS.setVodUrl(backgroundVideoUrl);
				} catch (error) {
					console.log(backgroundVideoUrl);
					$("body").css("background","transparent");
					// $("body").append('<video autoplay="autoplay" loop="loop"><source  src="'+backgroundVideoUrl+'" type="video/mp4" ></video>');
				}				
			}else if(backgroudimg !=""){
				$("body").css("background",'url("'+backgroudimg+'") no-repeat top center');
			}
			
			if(json.data.welcome.managerSignature != undefined){		
				var managerSignature = '<img src="' + IMGLIST.data[json.data.welcome.managerSignature.internalImageId].internal_image_url + '" />';
				$(".content .boss").html(managerSignature);
			}
			

			 $(".content .contentText").html(BR(json.data.translates[defaultLanguage].welcomeMessage));
	       
			$(".left img").attr("src", logo);

			localStorage.backgroudimg = backgroudimg;
			delete BASEDATA.page_type;
			delete BASEDATA.lang_agnostic_id;

			getWeather(defaultLanguage);
	    },
	
	    function (err) {
	        console.error("getWelcomePage: " + err.responseText);
	    }
	);
}


//6:获取天气预报接口
function getWeather(defaultLanguage){

	BASEDATA.page_type = 'info_page';
	BASEDATA.lang_agnostic_id = 'local_weather';
	BASEDATA.lang = defaultLanguage;
	var requestUrl = localStorage.apiServer + "/page/search/?" + $.param(BASEDATA);
	console.log('getWeather:', requestUrl);
	$.ajax(requestUrl).then(
	    function (json) {
				console.log("getWeather:", json);
				//保存天气预报图标和温度到临时缓存中
				localStorage.weather = JSON.stringify(json);
				sessionStorage.weather_imgurl = 'images/'+BASEDATA.hotel_id+'/wicon/' + json.data.weather_data.today.weatid + ' X42.png';
				sessionStorage.temperature = json.data.weather_data.today.temperature;
				var welcome_weather_imgurl = 'images/'+BASEDATA.hotel_id+'/wicon/' + json.data.weather_data.today.weatid + ' X42.png';
				//向页面渲染数据
				$(".weather span").html(json.data.weather_data.today.temperature);       
				$(".weather img").attr('src',  welcome_weather_imgurl);
				delete BASEDATA.page_type;
				delete BASEDATA.lang_agnostic_id;
				delete BASEDATA.lang;
	    },
	
	    function (err) {
	        console.error("getWeather: " + err.responseText);
	    }
	);
}
