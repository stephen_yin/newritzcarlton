# 说明 #

这个版本是之前ritzcarlton版本的升级版，在原有版本基础上，替换了接口对接部分，以及原有代码优化，bug修复。
将原版作为1.0版，此版版本号2.0

### 如何恢复原版? ###

* 由于接口逻辑变化,原版部分版面结构需要改动比较大,本版本将取消对原版本的兼容.
* 带有V2的js为Havensphere的后台接口（Junction）

### 版本更新历史i记录 ###
* 【2018-7-2】
    建立仓库： newRitzCalton
   git clone https://stephen_yin@bitbucket.org/stephen_yin/newritzcarlton.git
*  【2018-7-6】
    增加welcome.v2.js (对接新的junction接口))
    将原有接口另存为: welcome.v1.js
    如需还原，只需在welcome.html中开启welcome.v1.js.屏蔽welcome.v2.js即可。

* 【2018-7-19】
    1.更新欢迎页自适应屏幕的css文件 welcome_auto.css，以适应960*540px的webview分辨率；
    默认不开启。如需开启，在代码中中将注释的welcome_auto.css取消注释即可，同时屏蔽调welcome.css;

*  [2018-7-27]
    1.优化代码,去除原版本支持,因为原版本的代码布局因为接口逻辑不一样,改动太大,无法只通过修改JS文件就可以还原.
    2.完成了纯信息类型的页面接口对接.(标题,内容,和图片轮播).

*   [2018-7-30]
    1.完成天气预报页面的接口对接.
    2.完成VOD界面的开发及接口对接.

*   [2018-7-31]
    1.完成VOD列表页的焦点移动( 翻页部分需要针对电视机进行测试,暂未做处理 );